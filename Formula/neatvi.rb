class Neatvi < Formula
  desc "ex/vi clone for editing bidirectional uft-8 text"
  homepage "http://repo.or.cz/w/neatvi.git"
  head "git://repo.or.cz/neatvi.git"

  def patches
    [
      'https://gist.githubusercontent.com/telemachus/8c20ec58d186ce09634e/raw/345f9b67955f795f6ae5eea90c58e8c9f2e31752/neatvi.diff'
    ]
  end

  def install
    system "make"
    bin.install "vi" => "neatvi"
  end
end
