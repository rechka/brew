# rechka/brew

A [Homebrew][homebrew] tap for some of my favorite formulas.

Install the tap via `brew any-tap rechka/brew` and then `brew
cask install [options] <whatever>`.

[homebrew]: https://github.com/mxcl/homebrew/
